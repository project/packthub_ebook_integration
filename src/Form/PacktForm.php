<?php

namespace Drupal\packthub_ebook_integration\Form;

/**
 * @file
 * File PacktFrom contains PacktForm.
 */

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\packthub_ebook_integration\Plugin\Products;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class for saving token for packt api usage.
 *
 * @class
 * PacktForm for saving token.
 */
class PacktForm extends FormBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Messenger property.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new CustomFormExampleForm objects.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): PacktForm|static {
    return new self(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "ebook_api_token_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Packthub token'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $config = $this->configFactory->getEditable('packthub_ebook_integration.settings');
    $token = $form_state->getValue('api_token');
    if ($config->set('api_token', $token)->save()) {
      $product = new Products();
      $product->setFilter('product_type', '=', 'book');
      $products = $product->getProducts();
      if (empty($products['products'])) {
        $config->delete();
        $this->messenger->addError("Sorry failed to save packthub api token due to invalid token value");
      }
      else {
        $this->messenger->addMessage("Packthub api token saved");
        $url = Url::fromRoute('packthub_ebook_integration.products')->toString();
        (new RedirectResponse($url))->send();
        exit;
      }
    }
    else {
      $this->messenger->addError("Sorry failed to save packthub api token");
    }
  }

}
