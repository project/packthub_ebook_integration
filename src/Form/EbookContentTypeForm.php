<?php

namespace Drupal\packthub_ebook_integration\Form;

/**
 * @file
 * EbookContentTypeForm file contains EbookContentTypeForm class for map.
 */

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\packthub_ebook_integration\Plugin\EbookDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This is for mapping in advance the storage.
 *
 * @class
 * EbookContentTypeForm class for mapping content type with packt.
 */
class EbookContentTypeForm extends FormBase {

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Cache factory.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheFactory;

  /**
   * Constructs a new CustomFormExampleForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configs.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityType manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheFactory
   *   Cache factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager, CacheBackendInterface $cacheFactory) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $this->cacheFactory = $cacheFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EbookContentTypeForm|static {
    return new self(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "packt_ebook_content_type_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $configuration = new EbookDefinition($this->entityTypeManager, $this->cacheFactory);
    $configs = $configuration->contentTypeDefinition();
    $options = [];
    foreach ($configs as $config) {
      $options[$config['id']] = $config['label'];
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Types, Select any of these content types to be used for ebook creation'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'dynamic_fields_wrapper',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    // Dynamic fields wrapper.
    $form['dynamic_fields'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'dynamic_fields_wrapper'],
    ];

    // Additional fields that will be displayed based on the select field value.
    $selected_value = $form_state->getValue('content_type');
    if (!empty($selected_value)) {
      $this->fieldsCollection($selected_value, $configs, $form);
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $data = [];
    $notNeeded = ['op', 'form_token', 'form_id', 'form_build_id', 'submit'];
    foreach ($form_state->getValues() as $key => $value) {
      if (!in_array($key, $notNeeded) && !empty($value)) {
        $data[$key] = $value;
      }
    }
    $config = $this->configFactory->getEditable('packthub_ebook_integration.fields_settings');
    if ($config->set('field_sets', $data)->save()) {
      $this->messenger->addMessage("You have successfully saved configurations");
    }
    else {
      $this->messenger->addError("Failed to save the configuration");
    }
  }

  /**
   * Ajax callback to update dynamic fields based on the select field value.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Fields are returned.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['dynamic_fields'];
  }

  /**
   * Collecting fields of content type selected.
   *
   * @param string $type
   *   Content Type name.
   * @param array $configs
   *   Configuration mapping.
   * @param array $form
   *   Form array by reference.
   *
   * @return void
   *   Nothing.
   */
  private function fieldsCollection(string $type, array $configs, array &$form): void {

    $fields = [];
    foreach ($configs as $config) {
      if ($type === $config['id']) {
        $fields = $config['fields'];
        break;
      }
    }

    if (!empty($fields)) {
      foreach ($fields as $field) {
        $form['dynamic_fields'][$field['id']] = [
          '#type' => 'select',
          '#title' => $field['label'],
          '#options' => (new EbookDefinition($this->entityTypeManager, $this->cacheFactory))->packthubFields(),
          '#empty_option' => $this->t('- Select -'),
        ];
      }
    }
  }

}
