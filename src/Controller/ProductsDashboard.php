<?php

namespace Drupal\packthub_ebook_integration\Controller;

/**
 * @file
 * ProductsDashboard file contains controller for dashboard.
 */

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileStorage;
use Drupal\node\Entity\Node;
use Drupal\packthub_ebook_integration\Plugin\EbookDefinition;
use Drupal\packthub_ebook_integration\Plugin\EbookNodeProcessor;
use Drupal\packthub_ebook_integration\Plugin\Products;
use Drupal\packthub_ebook_integration\Plugin\ProductsAssets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * ProductsDashboard for controllers handling.
 *
 * @class ProductsDashboard This controller class is for dashboard.
 */
class ProductsDashboard extends ControllerBase {

  /**
   * Cache killer property.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected KillSwitch $killSwitch;

  /**
   * Cache backend interface property.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheFactory;

  /**
   * Messenger for status messages.
   *
   * @var messenger
   */
  protected $messenger;

  /**
   * Render for theme template building.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Url generator property.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * Module handler property.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Configuration factory access property.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * FileStorage property.
   *
   * @var \Drupal\file\FileStorage
   */
  private FileStorage $fileSystem;

  /**
   * FileSystemInterface property.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystemInterface;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    KillSwitch $kill_switch,
    RendererInterface $renderer,
    CacheBackendInterface $cacheFactory,
    MessengerInterface $messenger,
    ConfigFactoryInterface $config_factory,
    FileUrlGeneratorInterface $fileUrlGenerator,
    ModuleHandlerInterface $moduleHandler,
    FileStorage $fileSystem,
    FileSystemInterface $fileSystemInterface,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->killSwitch = $kill_switch;
    $this->renderer = $renderer;
    $this->cacheFactory = $cacheFactory;
    $this->configFactory = $config_factory;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->moduleHandler = $moduleHandler;
    $this->fileSystem = $fileSystem;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystemInterface = $fileSystemInterface;
    $this->messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):
  ProductsDashboard|static {
    return new self(
      $container->get('page_cache_kill_switch'),
      $container->get('renderer'),
      $container->get('cache.default'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('file_url_generator'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_system'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Listing ebooks products on dashboard.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return array
   *   Theme array is returned.
   *
   * @throws \Exception
   */
  public function products(Request $request): array {
    $this->killSwitch->trigger();

    $pagesTotal = $this->totalPages();

    $page = $request->get('page', 0);

    // Let's check in the cache for this page.
    $cacheId = 'packt_page_products_' . $page;
    $products = $this->cacheFactory->get($cacheId)->data ?? NULL;
    if (empty($products)) {

      // Lets get products.
      $prod = new Products();
      $prod->setLimit(9);
      $prod->setPage((int) $page);
      $prod->setFilter('product_type', '=', 'book');
      $products = $prod->getProducts(all: FALSE);
      $products = $this->processDetails($products, (int) $page);
    }

    $url = Url::fromRoute('packthub_ebook_integration.products_Search')->toString();
    $urlCreate = Url::fromRoute('packthub_ebook_integration.products_ebook')->toString();

    $loading = $this->moduleHandler->getModule('packthub_ebook_integration')->getPath() . "/assets/images/loading.gif";
    $loading = $this->fileUrlGenerator->generateAbsoluteString($loading);

    $create = $this->moduleHandler->getModule('packthub_ebook_integration')->getPath() . "/assets/images/creating.gif";
    $create = $this->fileUrlGenerator->generateAbsoluteString($create);

    return [
      '#theme' => 'packt_products_dashboard',
      '#title' => 'Dashboard',
      '#content' => [
        'products' => $products,
        'total' => count($products),
        'url' => $url,
        'icon' => $loading,
        'create' => $create,
        'create_url' => $urlCreate,
        'total_page' => $pagesTotal,
      ],
      '#attached' => [
        'library' => [
          'packthub_ebook_integration/manager_assets',
        ],
      ],
    ];
  }

  /**
   * Getting details of ebooks for api.
   *
   * @param array $products
   *   Products array ie ebooks.
   * @param int $page
   *   Page number.
   *
   * @return array
   *   Products details is returned.
   *
   * @throws \Exception
   */
  private function processDetails(array $products, int $page = 0): array {

    $details = [];
    foreach ($products as $product) {
      $pid = $product['product_id'] ?? NULL;

      if (!empty($pid)) {
        $pDetails = new Products();
        $pDetails->setProductId($pid);

        $productInfo = $pDetails->getProducts();
        $assets = new ProductsAssets();
        $assets->setProduct($pid);

        $png = [];
        foreach ($productInfo['files'] as $file) {
          if ($file['distribution_type'] === 'cover_image') {
            $png = $file;
            break;
          }
        }

        $thumbnail = NULL;
        $defaultImage = NULL;
        $noImage = FALSE;

        if ($png) {
          $filename = explode('.', $png['file_name'] ?? "");
          if (!empty($filename[0])) {

            $assets->setFileName($filename[0]);
            $ext = end($filename);
            $file = $assets->getAssets();

            if (!empty($file['data'])) {
              $thumbnail = "data:image/$ext;base64," . base64_encode($file['data']);
            }
            else {
              $noImage = TRUE;
            }
          }
        }

        if ($noImage || empty($png)) {
          $modulePath = $this->moduleHandler->getModule('packthub_ebook_integration')->getPath();
          $defaultImagePath = $modulePath . '/assets/images/book_coming_soon.png';
          $defaultImage = $this->fileUrlGenerator->generateAbsoluteString($defaultImagePath);
        }

        $details[] = [
          'product_name' => $productInfo['product_information']['title'] ?? '',
          'product_id' => $productInfo['product_id'] ?? NULL,
          'description' => $productInfo['product_information']['meta_description'] ?? NULL,
          'date' => (new \DateTime($productInfo['metadata']['publication_date'] ?? 'now'))->format('d F, Y'),
          'author' => $productInfo['contributors'][0]['firstname'] ?? NULL,
          'type' => ucfirst($productInfo['product_type']) ?? NULL,
          'thumbnail' => $thumbnail ?? $defaultImage,
        ];
      }
    }

    // Get the cache factory from the container.
    $cacheId = 'packt_page_products_' . $page;
    // Store the array in the cache.
    $this->cacheFactory->set($cacheId, $details, CacheBackendInterface::CACHE_PERMANENT);

    return $details;
  }

  /**
   * Searcher of ebooks.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json data returner.
   *
   * @throws \Exception
   */
  public function productsSearch(Request $request): JsonResponse {

    $this->killSwitch->trigger();

    $limit = $request->get('limit', 12);
    $type = $request->get('type', 'book');
    $title = $request->get('title');
    $page = $request->get('page', 1);
    $orderBy = $request->get('orderby', 'title');

    $buildUp = new Products();

    if (!empty($title)) {
      $buildUp->setFilter('title', '<=', $title);
    }

    if (!empty($type)) {
      $buildUp->setFilter('product_type', '=', $type);
    }

    if (!empty($page)) {
      $buildUp->setPage((int) $page);
    }

    if (!empty($limit)) {
      $buildUp->setLimit((int) $limit);
    }

    if (!empty($orderBy)) {
      $buildUp->setOrderBy($orderBy);
    }

    $products = $buildUp->getProducts(all: FALSE);

    if (!empty($products)) {
      $products = $this->processDetails($products, 0);
    }
    else {
      return (new JsonResponse([], 404));
    }
    $results = $this->createCards($products);
    return (new JsonResponse(['results' => $results, 'total' => count($products)], 200));
  }

  /**
   * Building cards html.
   *
   * @param array $products
   *   Products to build cards for.
   *
   * @return string
   *   Html of cards for ebooks.
   */
  private function createCards(array $products):string {
    $rendered_output = [
      '#theme' => 'packt_products_search',
      '#title' => NULL,
      '#content' => ['products' => $products],
      '#attached' => [
        'library' => [
          'packthub_ebook_integration/manager_assets',
        ],
      ],
    ];
    // Render the HTML output using the renderer service.
    return $this->renderer->renderRoot($rendered_output);
  }

  /**
   * Preparing node data to be save on click of confirmation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json data returner.
   *
   * @throws \Random\RandomException
   */
  public function prepareNodeCreation(Request $request): JsonResponse {
    // Get JSON data from the request.
    $jsonData = json_decode($request->getContent(), TRUE);
    $product = $jsonData['product'] ?? $request->get('product');

    if (!empty($product)) {
      $ebooks = new EbookNodeProcessor(explode(',', $product), $this->entityTypeManager, $this->fileSystemInterface);
      if ($ebooks->attemptFieldsMapping()) {
        $data = $ebooks->getMappedDataReady();
        $message = $ebooks->getMessages();
        $cacheId = random_int(100, 1000);
        $this->cacheFactory->set($cacheId, $data, CacheBackendInterface::CACHE_PERMANENT);
        $save = Url::fromRoute('packthub_ebook_integration.ebook_action', ['action' => 'save', 'id' => $cacheId])->toString();
        $delete = Url::fromRoute('packthub_ebook_integration.ebook_action', ['action' => 'delete', 'id' => $cacheId])->toString();
        return (new JsonResponse(['results' => $message, 'save' => $save, 'delete' => $delete], 200));
      }
      else {
        $url = Url::fromRoute('packthub_ebook_integration.ebook_configure')->toString();
        return (new JsonResponse(['results' => "Content Type not provided, please configure content type <a href='$url'>here</a> for storing ebooks"], 200));
      }
    }
    return (new JsonResponse(['results' => 'Product id (s) not provided'], 404));
  }

  /**
   * Finishing saving node object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return string[]
   *   Data of nodes created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function finishingCreations(Request $request): array {
    $this->killSwitch->trigger();
    $cacheId = $request->get('id');
    $action = $request->get('action');

    $nodes = $this->cacheFactory->get($cacheId)->data ?? NULL;

    if ($action === 'save') {
      if (!empty($nodes)) {
        foreach ($nodes as $node) {
          if ($node instanceof Node) {
            $node->save();
            $this->messenger->addMessage('Created ebook: ' . $node->getTitle());
          }
        }

        $url = Url::fromRoute('packthub_ebook_integration.products')->toString();
        $this->cacheFactory->delete($cacheId);
        (new RedirectResponse($url))->send();
      }
    }

    if ($action === 'delete') {
      $configuration = packthub_ebook_integration_ebook_maps();
      if ($configuration) {
        $contentType = $configuration['content_type'];
        unset($configuration['content_type']);
        $fields = array_keys($configuration);
        $ebook = new EbookDefinition($this->entityTypeManager, $this->cacheFactory);
        $fieldInfo = $ebook->contentFieldDefinitions($contentType);

        foreach ($fieldInfo as $item) {
          if (in_array($item['id'], $fields) && ($item['type'] === 'image' || $item['type'] === 'file_managed' || $item['type'] === 'file')) {

            // Lets look for node fid.
            foreach ($nodes as $node) {
              if ($node instanceof Node) {
                $fid = $node->get($item['id'])->getValue()[0]['target_id'] ?? NULL;
                if (!empty($fid)) {
                  $file = $this->fileSystem->load($fid);
                  $filename = $file->getFilename();

                  if ($file instanceof File) {
                    $file->delete();
                    $this->messenger->addMessage("Ebook related file removed ($filename)");
                  }
                }
              }
            }
          }
        }
        $this->cacheFactory->delete($cacheId);

        $this->messenger->addMessage("Ebook creation cancelled successfully");
        $url = Url::fromRoute('packthub_ebook_integration.products')->toString();
        $this->cacheFactory->delete($cacheId);
        (new RedirectResponse($url))->send();
      }
    }
    return ['#markup' => 'Something went wrong'];
  }

  /**
   * Total pages giver.
   *
   * @return int
   *   Number of pages.
   */
  public function totalPages(): int {
    $cacheId = 'packt_page_products_meta';

    // To retrieve the cached array:
    $meta = $this->cacheFactory->get($cacheId)->data ?? NULL;
    if (!empty($meta)) {
      return $meta;
    }
    $pr = new Products();
    $pr->setLimit(1);
    $pr->setFilter('product_type', '=', 'book');
    $product = $pr->getProducts();
    if (!empty($product) && $product['last_page']) {

      // Store the array in the cache.
      $this->cacheFactory->set($cacheId, $product['last_page'], CacheBackendInterface::CACHE_PERMANENT);
      return $product['last_page'];
    }
    return 100;
  }

}
