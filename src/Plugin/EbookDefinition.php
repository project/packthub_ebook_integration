<?php

namespace Drupal\packthub_ebook_integration\Plugin;

/**
 * @file
 * File EbookDefinition contains class EbookDefinition.
 */

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\node\Entity\NodeType;

/**
 * EbookDefinition class for handling definitions.
 *
 * @class EbookDefinition class for packt definitions.
 */
class EbookDefinition {

  /**
   * Entity manager property.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cachebackend property.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Construct for Ebook definitions.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, CacheBackendInterface $cacheBackend) {
    $this->entityTypeManager = $entityTypeManager;
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * Build up content type definitions.
   *
   * @return array
   *   Content type definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function contentTypeDefinition(): array {
    $types = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      if ($contentType instanceof NodeType) {
        $types[] = [
          'id' => $contentType->id(),
          'label' => $contentType->label(),
          'fields' => $this->contentFieldDefinitions($contentType->id()),
        ];
      }
    }
    return $types;
  }

  /**
   * Getting fields of  content type.
   *
   * @param string $content_type
   *   Content type name.
   *
   * @return array
   *   Content type fields.
   */
  public function contentFieldDefinitions(string $content_type): array {
    $field_storage_configs = $this->entityTypeManager
      ->getFieldDefinitions('node', $content_type);

    $fields = [];
    foreach ($field_storage_configs as $config) {
      if ($config instanceof FieldStorageDefinitionInterface || $config instanceof FieldDefinitionInterface) {
        $fields[] = [
          'label' => $config->getLabel(),
          'id' => $config->getName(),
          'type' => $config->getType(),
        ];
      }
    }
    return array_filter($fields, function ($item) {
      // Filter out system fields.
    });
  }

  /**
   * Getting fields of packthub.
   *
   * @return array|string[]
   *   Packt hub fields.
   */
  public function packthubFields(): array {
    $cacheId = 'packt_first_product';
    $dd = $this->cacheBackend->get($cacheId)->data ?? NULL;

    if (empty($dd)) {
      $pro = new Products();
      $pro->setLimit(1);
      $pro->setFilter('product_type', '=', 'book');
      $prod = $pro->getProducts(all: FALSE);
      $pro->setProductId($prod[0]['product_id']);
      $dd = $pro->getProducts();
      // Store the array in the cache.
      $this->cacheBackend->set($cacheId, $dd, CacheBackendInterface::CACHE_PERMANENT);
    }

    if (!empty($dd)) {
      $fields = [
        'product_id' => 'Product ID',
        'product_type' => 'Product Type',
        'distribution_status' => 'Distribution status',
        'early_access' => 'Early access',
        'sample_chapter' => 'Sample chapter',
        'isbn' => 'ISBN',
      ];

      foreach ($dd as $key => $item) {
        if ($key === 'metadata') {
          $fields['metadata.category.name'] = 'Product category';
          $fields['metadata.publication_date'] = 'Publication date';
          $fields['metadata.concept.name'] = 'Product concept';
          $fields['metadata.edition.edition'] = 'Product edition';
        }

        if ($key === 'product_information') {
          $fields['product_information.title'] = 'Product title';
          $fields['product_information.subtitle'] = 'Product subtitle';
          $fields['product_information.short_description'] = 'Product short description';
          $fields['product_information.description'] = 'Product description';
          $fields['product_information.meta_description'] = 'Product meta description';
          $fields['product_information.publisher'] = 'Product publisher';
          $fields['product_information.publishing_dept'] = 'Product publishing dept';
          $fields['product_information.what_you_will_learn'] = 'Product what you will learn';
          $fields['product_information.audience'] = 'Product audience';
          $fields['product_information.approach'] = 'Product approach';
          $fields['product_information.key_features'] = 'Product key features';
          $fields['product_information.github_repo'] = 'Product github repo';
          $fields['product_information.imprint'] = 'Product imprint';
          $fields['product_information.keywords'] = 'Product keywords';
        }

        if ($key === 'contributors') {
          $fields['contributors.0.firstname'] = 'Author First name';
          $fields['contributors.0.lastname'] = 'Author Last name';
          $fields['contributors.0.description'] = 'Author description';
          $fields['contributors.0.linkedin_url'] = 'Author linkedin url';
          $fields['contributors.0.profile_url'] = 'Author profile url';
        }

        if ($key === 'chapters') {
          $fields['chapters.chapter_name'] = 'Chapters names (| seperated)';
          $fields['chapters.chapter_number'] = 'Chapters numbers (| separated)';
        }

        if ($key === 'files') {
          $fields['files.distribution_type.cover_image'] = 'Ebook cover image';
          $fields['files.distribution_type.epub'] = 'Ebook epub file';
          $fields['files.distribution_type.ebook'] = 'Ebook pdf file';
        }

        if ($key === 'formats') {
          $fields['formats.0.ebook.price'] = 'Ebook prices (| separated)';
        }
      }
      return $fields;
    }
    return [];
  }

}
