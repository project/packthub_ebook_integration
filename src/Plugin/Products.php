<?php

namespace Drupal\packthub_ebook_integration\Plugin;

/**
 * @file
 * Products file contains Products class.
 */

/**
 * Products used for ebook listing.
 *
 * @class
 * Class Products used to get ebooks.
 */
class Products {

  /**
   * Order by property.
   *
   * @var string
   */
  private string $orderBy = 'title';

  /**
   * Filter property.
   *
   * @var array
   */
  private array $filters = [];

  /**
   * Page count.
   *
   * @var int
   */
  private int $pageCount = 1;

  /**
   * Limit of content.
   *
   * @var int
   */
  private int $limit = 10;

  /**
   * Raw response.
   *
   * @var array
   */
  private array $response;

  /**
   * Product id.
   *
   * @var string
   */
  private string $productID;

  /**
   * Getting raw response.
   *
   * @return array
   *   Raw response array.
   */
  public function getResponse(): array {
    return $this->response;
  }

  /**
   * Getting products processed.
   *
   * @param bool $pageActual
   *   If is set to true, the result will be of giving page example page 4.
   *   If false
   *   results are from page 1 to 4.
   * @param bool $all
   *   True all data returned from api call will be given if a false only
   *   products list
   *   is given if not other metadata is given.
   *
   * @return array
   *   Product listing or product details.
   */
  public function getProducts(bool $pageActual = TRUE, bool $all = TRUE): array {

    if ($pageActual) {
      $results = $this->runCurl($this->pageCount)->getResponse();
      if ($all) {
        return $results;
      }
      return !empty($results['products']) ? $results['products'] : [
        'current_page' => $results['current_page'] ?? 0,
        'from' => $results['from'] ?? 0,
        'last_page' => $results['last_page'] ?? 0,
        'per_page' => $results['per_page'] ?? 0,
        'to' => $results['to'] ?? 0,
      ];
    }

    $data = [];
    for ($i = 1; $i <= $this->pageCount; $i++) {
      $results = $this->runCurl($i)->getResponse();
      if ($all) {
        $data[] = $results;
      }
      else {
        $data[] = !empty($results['products']) ? $results['products'] : [
          'current_page' => $results['current_page'] ?? 0,
          'from' => $results['from'] ?? 0,
          'last_page' => $results['last_page'] ?? 0,
          'per_page' => $results['per_page'] ?? 0,
          'to' => $results['to'] ?? 0,
        ];
      }
    }
    return $data;
  }

  /**
   * Run curl function.
   *
   * @param int $page
   *   Page number.
   *
   * @return \Drupal\packthub_ebook_integration\Plugin\Products
   *   Returns Products instance.
   */
  private function runCurl(int $page = 1): Products {

    // Build up params string.
    $params = [];
    $api = "https://api.packt.com/api/v2/products";

    // Add product id if exist.
    if (!empty($this->productID)) {
      $api .= "/$this->productID";
    }

    $params[] = "token=" . packthub_ebook_integration_api_token();

    // Add page number.
    $params[] = "page=$page";

    // Add limit.
    $params[] = "limit=$this->limit";

    $params[] = "orderBy=$this->orderBy";

    $params[] = "orderType=";

    // Add filters.
    foreach ($this->filters as $filter) {
      if (!empty($filter['name']) && !empty($filter['operator']) && !empty($filter['value'])) {
        $v = urlencode($filter['value']);
        $params[] = "filter[name]={$filter['name']}&filter[operator]={$filter['operator']}&filter[value]={$v}";
      }
    }

    // Joining all params.
    $paramsLine = implode('&', $params);
    $url = trim("$api?$paramsLine", '?');

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ]);
    $d = curl_exec($curl);
    $this->response = !empty($d) ? json_decode($d, TRUE) : [];
    curl_close($curl);
    return $this;
  }

  /**
   * Setting order by.
   *
   * @param string $key
   *   Title,published_from, language, product_type.
   *
   * @return void
   *   Nothing
   */
  public function setOrderBy(string $key): void {
    $keys = ['product_type', 'title', 'published_from', 'language'];
    if (in_array($key, $keys)) {
      $this->orderBy = $key;
    }
  }

  /**
   * Setting page number.
   *
   * @param int $page
   *   Number of page or pages.
   *
   * @return void
   *   Nothing.
   */
  public function setPage(int $page): void {
    $this->pageCount = $page;
  }

  /**
   * Setting limit number.
   *
   * @param int $limit
   *   Returned records count.
   *
   * @return void
   *   Nothing.
   */
  public function setLimit(int $limit): void {
    $this->limit = $limit;
  }

  /**
   * Setting order type.
   *
   * @param string $value
   *   Order type value.
   *
   * @return void
   *   Nothing.
   */
  public function setOrderType(string $value): void {
    if (in_array(strtoupper($value), ['DESC', 'ASC'])) {
      // $this->orderType = strtoupper($value);
    }
  }

  /**
   * Setting filters.
   *
   * @param string $name
   *   Accepted values for name: product_type, title, published_from, language.
   * @param string $operator
   *   Accepted Values for operator: =, <, <=, >, >=, !=.
   * @param int|string|bool $value
   *   Value of filter.
   *
   * @return void
   *   Nothing.
   */
  public function setFilter(string $name, string $operator, int|string|bool $value): void {
    $names = ['product_type', 'title', 'published_from', 'language'];
    if (in_array(strtolower($name), $names)) {
      $operators = ['=', '<', '<=', '>', '>=', '!=', 'IS NULL',
        'IS NOT NULL', 'RANGE', 'ANY', 'ALL', 'NOT ANY', 'NOT ALL',
      ];
      if (in_array(strtoupper($operator), $operators)) {
        $this->filters[] = [
          'name' => $name,
          'operator' => strtoupper($operator),
          'value' => $value,
        ];
      }
    }
  }

  /**
   * Setting product id for details collection.
   *
   * @param string $product
   *   Product ID or ISBN.
   *
   * @return void
   *   Nothing.
   */
  public function setProductId(string $product): void {
    $this->productID = $product;
  }

}
