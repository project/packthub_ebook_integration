<?php

namespace Drupal\packthub_ebook_integration\Plugin;

/**
 * @file
 * File EbookNodeProcessor contains EbookNodeProcessor class.
 */

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;

/**
 * EbookNodeProcessor is used to process the node of ebooks.
 *
 * @class
 * EbookNodeProcessor is for ebook node processing.
 */
class EbookNodeProcessor {

  /**
   * Messenger property.
   *
   * @var array
   */
  private array $messages;

  /**
   * Mapped data.
   *
   * @var array
   */
  private array $mappedDataReady;

  /**
   * FileSystemInterface property.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * EntityTypeManager property.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Construct for EbookNodeProcessor class.
   *
   * @param array $product_ids
   *   Products id.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityTypeManager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   FileSystem.
   */
  public function __construct(private readonly array $product_ids, EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem) {
    $this->messages = [];
    $this->mappedDataReady = [];
    $this->fileSystem = $fileSystem;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * All massages of process occurred.
   *
   * @return array
   *   Messages array
   */
  public function getMessages(): array {
    return $this->messages;
  }

  /**
   * Getting mapped data made.
   *
   * @return array
   *   Mapped data from processing occurred.
   */
  public function getMappedDataReady(): array {
    return $this->mappedDataReady;
  }

  /**
   * Attempting to map data based of configuration given.
   *
   * @return bool|null
   *   True if mapping was success.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function attemptFieldsMapping(): bool|null {
    $fields = packthub_ebook_integration_ebook_maps();
    if (empty($fields)) {
      return NULL;
    }

    $contentType = $fields['content_type'];
    unset($fields['content_type']);

    $mappedProducts = [];

    $product = new Products();

    foreach ($this->product_ids as $key => $product_id) {

      if (!empty($product_id)) {
        $product->setProductId($product_id);
        $details = $product->getProducts();
        if (!empty($details)) {

          foreach ($fields as $k => $field) {

            if (!strpos($field, '.')) {
              $mappedProducts[$key][$k] = $details[$field] ?? NULL;
            }
            else {
              $list = explode('.', $field);
              $result = $details;
              foreach ($list as $part) {

                if ($part === 'files') {
                  $res = $this->findFilesRequired($list, $details['files'], $details['product_id'], $details['product_information']['title'] ?? '');

                  if (!empty($res)) {
                    $result = $res;
                  }
                  else {
                    $this->messages[] = 'File processing failed';
                  }
                  break;
                }
                elseif ($part === 'chapters') {
                  $result = $this->makeChapters($list, $details['chapters']);
                  break;
                }
                elseif (isset($result[$part])) {
                  $result = $result[$part];
                }
              }
              $mappedProducts[$key][$k] = $result;
            }
          }
        }
      }
    }

    if (!empty($mappedProducts)) {
      foreach ($mappedProducts as $key => $mappedProduct) {
        $mappedProduct['type'] = $contentType;
        $node = Node::create($mappedProduct);
        $node->enforceIsNew();
        $this->mappedDataReady[] = $node;
        $this->messages[] = $key . ' - Product prepared successfully';
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Write and processing ebooks files and images.
   *
   * @param array $list
   *   Products.
   * @param array $files
   *   Files paths.
   * @param string $product
   *   Product id.
   * @param string $productTitle
   *   Product title.
   *
   * @return array
   *   Array of public:// path is returned.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function findFilesRequired(array $list, array $files, string $product, string $productTitle = ''): array {
    $type = end($list);
    $middle = $list[1];
    $found = [];
    foreach ($files as $file) {
      if (isset($file[$middle]) && $file[$middle] === $type) {
        $found = $file;
        break;
      }
    }

    if (!empty($found)) {
      // Write file here.
      $assets = new ProductsAssets();
      $filename = $found['file_name'] ?? NULL;
      if ($filename) {
        $list = explode('.', $filename);
        $assets->setProduct($product);
        $assets->setFileName($list[0]);
        $data = $assets->getAssets();

        if (!empty($data['data']) && !empty($data['mime_type'])) {
          $extension = explode('/', $data['mime_type']);
          $extension = end($extension);

          $public = "public://ebook_covers";

          if (!is_dir($public)) {
            // Ensure the directory exists.
            $this->fileSystem->prepareDirectory($public, FileSystemInterface::CREATE_DIRECTORY);
          }

          $filename = $productTitle ?? uniqid();
          $filename = $this->prepareFilename($filename) . ".$extension";

          // Create a new file entity.
          $file = File::create([
            'uri' => $public . '/' . $filename,
            'status' => FileInterface::STATUS_PERMANENT,
          ]);

          // Save the file data.
          $this->fileSystem->saveData($data['data'], $file->getFileUri(), FileSystemInterface::EXISTS_REPLACE);

          // Save the file entity.
          $fid = $file->save();
          if ($fid) {
            if (str_contains($file->getMimeType(), 'image')) {
              // Get all image styles.
              $imageStyles = ImageStyle::loadMultiple();

              // Generate derivative images for each style.
              foreach ($imageStyles as $style) {
                // Generate a derivative image for the current style.
                $derivativeUri = $style->buildUri($file->getFileUri());
                $style->createDerivative($file->getFileUri(), $derivativeUri);
              }
            }
            return ['target_id' => $file->id()];
          }
        }
      }
    }
    return [];
  }

  /**
   * Making chapter string.
   *
   * @param array $list
   *   Chapters ids.
   * @param mixed $chapters
   *   Chapters array.
   *
   * @return string
   *   Chapter line is returned.
   */
  private function makeChapters(array $list, mixed $chapters): string {
    $type = end($list);
    $line = '';
    foreach ($chapters as $chapter) {
      if (!empty($chapter[$type])) {
        $line .= $chapter[$type] . '|';
      }
    }
    return substr($line, 0, strlen($line) - 1);
  }

  /**
   * Prepare title of ebook to name to use for image path.
   *
   * @param string $title
   *   Ebook title.
   *
   * @return string
   *   joined word by _ is returned.
   */
  private function prepareFilename(string $title): string {
    $clean = preg_replace('/[^a-zA-Z0-9]/', '_', $title);
    return strtolower(str_replace(' ', '_', $clean));
  }

}
