<?php

namespace Drupal\packthub_ebook_integration\Plugin;

/**
 * @file
 * ProductsAssets file contains class ProductsAssets.
 */

/**
 * ProductsAssets is used to get ebook file from packt.
 *
 * @class
 * ProductsAssets class for getting ebook files.
 */
class ProductsAssets {

  /**
   * Product id.
   *
   * @var string
   */
  private string $product;

  /**
   * File name to get.
   *
   * @var string
   */
  private string $fileName;

  /**
   * Getting file distro.
   *
   * @return string
   *   File name
   */
  public function getFileName(): string {
    return $this->fileName;
  }

  /**
   * Setting product id.
   *
   * @param string $product
   *   Give product id or isbn.
   *
   * @return void
   *   Nothing.
   */
  public function setProduct(string $product): void {
    $this->product = $product;
  }

  /**
   * Setting file name to get.
   *
   * @param string $fileName
   *   Filename of the file if we have multiple files uploaded for single
   *   distribution type.
   *
   * @return void
   *   Nothing.
   */
  public function setFileName(string $fileName): void {
    $this->fileName = $fileName;
  }

  /**
   * Collect file found.
   *
   * @param bool $resolveType
   *   True will return array of mime_type and data. False
   *   will return data stream.
   *
   * @return mixed
   *   Assets data.
   */
  public function getAssets(bool $resolveType = TRUE):mixed {

    $token = packthub_ebook_integration_api_token();
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.packt.com/api/v2/products/$this->product/$this->fileName?token=$token",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ]);
    $response = curl_exec($curl);
    curl_close($curl);

    if (!empty($response) && $resolveType) {
      // Create a FileInfo object.
      $info = finfo_open(FILEINFO_MIME_TYPE);

      // Get the MIME type directly from the binary data.
      $mime_type = finfo_buffer($info, $response);

      // Close the FileInfo object.
      finfo_close($info);
      return ['mime_type' => $mime_type, 'data' => $response];
    }
    return $response;
  }

}
