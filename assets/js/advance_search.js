const customSelects = document.querySelectorAll("select");
const deleteBtn = document.getElementById('delete')
const form = document.getElementById('search-products');
const searchArea = document.getElementById('products-cards');
const icon = form.getAttribute('icon');
const total = document.getElementById('total');
const choices = new Choices('select',
  {
    searchEnabled: FALSE,
    removeItemButton: true,
    itemSelectText: ''
  });
for (let i = 0; i < customSelects.length; i++) {
  customSelects[i].addEventListener('addItem', function (event) {
    if (event.detail.value) {
      let parent = this.parentNode.parentNode
      parent.classList.add('valid')
      parent.classList.remove('invalid')
    }
    else {
      let parent = this.parentNode.parentNode
      parent.classList.add('invalid')
      parent.classList.remove('valid')
    }
  }, FALSE);
}
deleteBtn.addEventListener("click", function (e) {
  e.preventDefault()
  const deleteAll = document.querySelectorAll('.choices__button')
  for (let i = 0; i < deleteAll.length; i++) {
    deleteAll[i].click();
  }
});


if(form) {
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    waiting();
    sendForSearch(extractElements(form));
  });
}

function extractElements(event) {
  const formData = new FormData(event);
  const inputInfo = [];

  // Loop through form elements
  for (const pair of formData.entries()) {
    const input = pair[0];
    const value = pair[1];
    if(input && value) {
      // Store input name, ID, and value in the object
      inputInfo.push({
        name: input,
        value: value,
      });
    }
  }
  return inputInfo;
}

function sendForSearch(data) {
  let params = "";
  data.forEach((item) => {
    const name = item.name;
    const value = item.value;
    params += name + '=' + value + '&';
  });
  params = params.substring(0, params.length - 1);

  const url = form.getAttribute('url');
  const xhr = new XMLHttpRequest();
  xhr.open('GET', url + '?' + params, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function () {
    if(this.status === 200) {
      if(searchArea) {
        try{
          const data = JSON.parse(this.responseText);
          searchArea.removeAttribute('style');
          searchArea.innerHTML = data.results;
          total.textContent = data.total;
        }catch (e) {
          console.error(e.message);
        }
      }
    }
  }
  xhr.send();
}

function waiting() {
  searchArea.innerHTML = '';
  const div = document.createElement('div');
  div.className = 'loading';
  const img = document.createElement('img');
  img.src = icon;
  img.className = 'loading_image';
  div.appendChild(img);
  searchArea.appendChild(div);
  searchArea.setAttribute('style', 'width:fit-content !important; display:flex !important;');
}

document.addEventListener('DOMContentLoaded', () => {
  sorter()
})

function sorter() {
  setTimeout(() => {
    const sortableAreas = ['#pages', '#limits'];
    for (let i = 0; i < sortableAreas.length; i++) {
      let divs = document.querySelector(sortableAreas[i]);
      if(divs) {
        divs = divs.querySelectorAll('.choices__list--dropdown > .choices__list > .choices__item--choice');
        // Convert NodeList to an array for sorting.
        const divsArray = Array.from(divs);
        // Sort the array based on the data-value attribute.
        divsArray.sort((a, b) => {
          const aValue = parseInt(a.getAttribute('data-value'), 10);
          const bValue = parseInt(b.getAttribute('data-value'), 10);
          return aValue - bValue;
        });

        // Clear the existing content of the parent container.
        let parentContainer = document.querySelector(sortableAreas[i]);
        if(parentContainer) {
          parentContainer = parentContainer.querySelector('.choices__list--dropdown > .choices__list');
          if(parentContainer) {
            parentContainer.innerHTML = '';
            // Append the sorted elements back to the parent container.
            divsArray.forEach((div) => {
              parentContainer.appendChild(div);
            });
          }
        }

      }
    }
  },10000)
}

// Select the form element
const formTag = document.querySelector('form');

// Options for the observer (which mutations to observe)
const config = { attributes: true, childList: true, subtree: true };

// Callback function to execute when mutations are observed
const callback = function (mutationsList, observer) {
  sorter();
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the form for configured mutations
observer.observe(formTag, config);

// Later, you can stop observing
// observer.disconnect();
