const multiTag = document.getElementById('multi-button');
const checkboxes = document.getElementsByClassName('checkbox');
if(checkboxes.length > 0) {
  const total = checkboxes.length;
  for (let i = 0; i < total; i++) {
    const checkbox = checkboxes[i];
    if(checkbox) {
      checkbox.addEventListener('change', (e) => {
        if(e.target.checked && !multiTag.querySelector('#mult_btn_create')) {
          // create button here
          const button = document.createElement('button');
          button.className = 'multi-ebook-create';
          button.type = 'button';
          button.textContent = 'Process selected products';
          button.id = 'mult_btn_create';
          button.addEventListener('click', handleMultiProducts);
          multiTag.appendChild(button);
        }
        else {
          let flag = FALSE;
          for (let j = 0; j < total; j++) {
            if(checkboxes[j].checked) {
              flag = true;
            }
          }
          if(flag === FALSE) {
            if(multiTag.querySelector('#mult_btn_create')) {
              multiTag.querySelector('#mult_btn_create').remove();
            }
          }
        }
      })
    }
  }
}

function openingModal(id, title, description) {
  const dialogHtml = document.getElementById('dialog');
  if(dialogHtml) {
    const h2_tag = dialogHtml.querySelector('h2');
    const p_tag = dialogHtml.querySelector('p');
    const button_tag = dialogHtml.querySelector('#create-action');
    if(h2_tag) {
      h2_tag.textContent = title;
    }
    if(p_tag) {
      p_tag.textContent = description;
    }
    if(button_tag) {
      button_tag.setAttribute('product', id);
      button_tag.addEventListener('click',(e) => createNodeAction(button_tag));
    }
  }
  window.dialog.showModal();
}

function createNodeAction(button_tag) {
  if(button_tag) {
    const product_id = button_tag.getAttribute('product');
    if(product_id) {
      const icon = button_tag.getAttribute('icon');
      const url = button_tag.getAttribute('url');
      if(icon && url) {
        button_tag.innerHTML = ` <img src = "${icon}" alt = "creating..">`;
        button_tag.setAttribute('style', 'background-color: white;');

        // Send backend call here.
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url + '?product=' + product_id, true);
        xhr.onload = function () {
          if(this.status === 200) {
            const results = JSON.parse(this.responseText);
            const dialogHtml = document.getElementById('dialog');
            const actions_tag = document.getElementById('actions-ebook');
            if(dialogHtml && actions_tag) {
              dialogHtml.querySelector('#create-action').remove();
              const messages = results.results;

              for (let i = 0; i < messages.length; i++) {
                const div = document.createElement('div');
                div.className = 'prepare_message';
                div.textContent = messages[i];
                actions_tag.appendChild(div);
              }
              const saveLink = document.createElement('a');
              saveLink.href = results.save;
              saveLink.className = 'finish-save';
              saveLink.textContent = 'Save';
              const deleteLink = document.createElement('a');
              deleteLink.className = 'finish-delete';
              deleteLink.href = results.delete;
              deleteLink.textContent = 'Cancel'
              const div = document.createElement('div');
              div.className = 'action-buttons';
              div.appendChild(saveLink);
              div.appendChild(deleteLink);
              actions_tag.appendChild(div);
            }
          }
          if(this.status === 404) {
            const message = JSON.parse(this.responseText).results;
            button_tag.innerHTML = ` <span> ${message} </span> `;
          }
        }
        //xhr.send(JSON.stringify({product: product_id}));
        xhr.send();
      }
    }
  }
}

function handleMultiProducts() {
  let products = [];
  for (let i = 0; i < checkboxes.length; i++) {
    if(checkboxes[i].checked) {
      products.push(checkboxes[i].value);
    }
  }
  if(products) {
    const ids = products.join(',');
    openingModal(ids,'Products selected: ' + ids, "You have selected " + ids +
     " for ebook creation click button below to start the process.");
  }
}
