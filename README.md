# Packthub E-Book Integration Module

The Packthub E-Book Integration module is a powerful solution designed to
seamlessly integrate the Packthub API with your Drupal 9 and 10 websites.
This module enables the incorporation of Packthub's extensive e-book catalog
directly into your Drupal environment, enhancing your content creation and
management processes with a wide range of features.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/packthub_ebook_integration).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/packthub_ebook_integration).


## Table of contents
- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following API:

- [Packthub API key](https://www.packtpub.com/)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to the configuration page _/admin/config/packthub/ebook_integration_
2. Enter your Packthub API key in the provided field.
3. Configure additional settings according to your needs, such as category
   selection, display options, and synchronization settings.
4. Save your configuration to apply the changes.

## Usage

After configuring the module, you can start adding e-book listings to your site.
Use the module's block and views integration to display e-book content on pages,
sidebars, or any other regions supported by your theme. Customize the appearance
and behavior of the e-book listings through the Drupal admin interface.


## Maintainers

- Chance Nyasulu - [chancenyasulu](https://www.drupal.org/u/chancenyasulu)
